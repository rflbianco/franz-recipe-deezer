# Deezer recipe for Franz

Custom [recipe](https://github.com/meetfranz/plugins) to support [Deezer](https://deezer.com) web player as a service on
[Franz](https://meetfranz.com).

### How to install

1. Clone this git repository into **Custom Services Folder**. You can check the exact location on your system going
    in `Settings -> Available Services -> Custom Services`.

    Example on Linux:
    ```shell
    cd ~/.config/Franz/recipes/dev

    git clone https://github.com/rflbianco/franz-recipe-deezer deezer
    ```

1. Restart Franz.

### Adding the service

Once installed, the service should be available to you under `Settings -> Available Services -> Custom Services`.
You can add it as any other Franz service/recipe.
